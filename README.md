# OpenPGP

OpenPGP is an open and widely-used standard for secure communication
and data encryption. It provides a framework for encryption, digital
signatures, and key management, enabling users to protect the
confidentiality, integrity, and authenticity of their messages and
files.

This crate does not provide any implementation of the standard but
rather links to existing Rust implementations in alphabetical order.

## Implementations

  - [pgp](https://crates.io/crates/pgp)
  - [sequoia-openpgp](https://crates.io/crates/sequoia-openpgp)
